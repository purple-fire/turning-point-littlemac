/**
 * @file
 * Global configuration.
 *
 * @date 2018-12-27
 * @copyright Copyright (c) 2018 Purple Fire Robotics
 */

#include "main.h"

pros::Controller master(pros::E_CONTROLLER_MASTER);

pros::Motor motorDriveLeft(1, MOTOR_GEARSET_18, false);
pros::Motor motorDriveRight(2, MOTOR_GEARSET_18, true);
pros::Motor motorCatapultLeft(3, MOTOR_GEARSET_18, false);
pros::Motor motorCatapultRight(4, MOTOR_GEARSET_18, true);
pros::Motor motorIntake(5, MOTOR_GEARSET_18, false);

pros::ADIButton btnCatapultLimit('B');

